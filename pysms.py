#!/usr/bin/python

import time
import serial

recipient = "+48886454684"
message = "hello arduino"

phone = serial.Serial("/dev/tty.usbmodem1421",  9600, timeout=5)

print "pysms..."

try:
    time.sleep(0.5)
    phone.write(b'ATZ\r')
    time.sleep(0.5)
    phone.write(b'AT+CMGF=1\r')
    time.sleep(0.5)
    phone.write(b'AT+CMGS="' + recipient.encode() + b'"\r')
    time.sleep(0.5)
    phone.write(message.encode() + b"\r")
    time.sleep(0.5)
    phone.write(bytes([26]))
    time.sleep(0.5)
finally:
    phone.close()

print "done"