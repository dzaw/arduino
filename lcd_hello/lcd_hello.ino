#include <LiquidCrystal.h>
LiquidCrystal lcd(2, 3, 4, 5, 6, 7); 
 
void setup() {
  lcd.begin(16, 2);
  lcd.setCursor(0, 0); //Ustawienie kursora
  lcd.print("hello arduino :) to jest test..."); //Wyświetlenie tekstu

}
 
void loop() {
  lcd.setCursor(0, 1); //Ustawienie kursora
  lcd.print(millis()/1000); //Wyświetlenie tekstu
  
  //lcd.noDisplay();
  //delay(500);
  //lcd.display();
  //delay(500);
  
  delay(500);
  
  for (int positionCounter = 0; positionCounter < 50; positionCounter++) {  
    // scroll one position left:
    lcd.scrollDisplayLeft();
    // wait a bit:
    delay(300);
  }

/*
  // scroll 29 positions (string length + display length) to the right
  // to move it offscreen right:
  for (int positionCounter = 0; positionCounter < 29; positionCounter++) {
    // scroll one position right:
    lcd.scrollDisplayRight();
    // wait a bit:
    delay(150);
  }


  // scroll 16 positions (display length + string length) to the left
  // to move it back to center:
  for (int positionCounter = 0; positionCounter < 29; positionCounter++) {
    // scroll one position left:
    lcd.scrollDisplayLeft();
    // wait a bit:
    delay(150);
  }
  */

  // delay at the end of the full loop:
  delay(1000);
}
