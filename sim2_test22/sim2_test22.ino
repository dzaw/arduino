#include <SoftwareSerial.h>
#include <Regexp.h>
SoftwareSerial mySerial(2,3);
SoftwareSerial mySerial2(8,9);

MatchState ms;
String buf;
char buf2[100];

/*
char data [21];
int number_of_bytes_received;
*/

void setup() {
  Serial.begin(9600);
  while(!Serial){};                 
  mySerial.begin(9600);
  mySerial2.begin(9600);
  delay(1000);
  mySerial.write("AT\n");
  delay(1000);  
}

void loop() {
  twoPortsReceive();
  //delay(500);
  //matchME(buf); // doesnt work here
  //catchME();
}

void twoPortsReceive(){
  mySerial.listen();
  if(mySerial.read() > 0) {
    Serial.println("Data from port one:");
    delay(500); // needed for Serial.write(inByte) and regex..
    while (mySerial.available() > 0) {
      char inByte = mySerial.read();
      Serial.write(inByte); // write this or write from matchME()
      buf += inByte;
      //return buf; // don't think return works
      matchME(buf); // here works but is done for every byte..
    }  
    //matchME(buf); // works
  }
  

  mySerial2.listen();
  if(mySerial2.read() > 0) {
    Serial.println("Data from port two:");
    while (mySerial2.available() > 0) {
      char inByte = mySerial2.read();
      Serial.write(inByte);
    }
  }
}

void matchME(String buf) {
    //Serial.println(sizeof(buf));
    buf.toCharArray(buf2, 100);
    ms.Target (buf2);
    //Serial.print(buf2);

    // find calling number

    char result = ms.Match ("+48......684");
      
      if (result > 0)
        {
        Serial.println();
        Serial.println("calling number:");
        Serial.println(buf.substring(ms.MatchStart, ms.MatchStart+ms.MatchLength));
        Serial.println();
        //mySerial.write("ATA\n"); // answers the call
        //mySerial.write("AT+CCFC=1,3,\"+48.........\""); // ?????????????? forward call.. not working
        }
        
    //redirect call AT+CCFC=...
    // model ver ATI
    // current conf AT&V

    /*

      char result = ms.Match ("CLIP");
      
      if (result > 0)
        {
        Serial.print ("Found match at: ");
        Serial.println (ms.MatchStart);   
        Serial.print ("Match length: ");
        Serial.println (ms.MatchLength); 
        }
     */
}

/*

void matchME()
{
  // match state object
  MatchState ms;

  // what we are searching (the target)
  char buf [100] = "The quick brown fox jumps over the lazy wolf";
  ms.Target (buf);  // set its address
  Serial.println (buf);

  char result = ms.Match ("f.x");
  
  if (result > 0)
    {
    Serial.print ("Found match at: ");
    Serial.println (ms.MatchStart);        // 16 in this case     
    Serial.print ("Match length: ");
    Serial.println (ms.MatchLength);       // 3 in this case
    }
  else
    Serial.println ("No match.");
    
}

void catchME(){
   if(Serial.available() > 0)
  {
   number_of_bytes_received = Serial.readBytesUntil (13,data,20); // read bytes (max. 20) from buffer, untill <CR> (13). store bytes in data. count the bytes recieved.
   data[number_of_bytes_received] = 0; // add a 0 terminator to the char array
  } 

  bool result = strcmp (data, "CLIP");
  // strcmp returns 0; if inputs match.

  if (result == 0)
  {
   Serial.println("!!!!!!!!!!data matches CLIP");
  }
  else
  {
   //Serial.println("data does not match @");
  }

}

void sim1(){
  if (mySerial.available()) {
    Serial.write(mySerial.read());
  } 
  if (Serial.available()) {
    mySerial.write(Serial.read());  
  }  
}
*/
